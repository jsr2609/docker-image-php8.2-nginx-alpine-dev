build:
	docker build -f $(shell pwd)/Dockerfile -t jsr2609/php8.2-nginx-alpine-dev:latest .
push:
	docker push jsr2609/php8.2-nginx-alpine-dev:latest
run:
	docker run --rm -d --name testphp -p 8090:80 jsr2609/php8.2-nginx-alpine-dev