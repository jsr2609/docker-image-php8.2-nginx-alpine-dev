#docker-sf54-php74fpm-alpine-dev
FROM php:8.2-fpm-alpine
# https://hub.docker.com/_/php

# If you are having difficulty figuring out which Debian or Alpine packages need to be installed before docker-php-ext-install, then have a look at the install-php-extensions project. This script builds upon the docker-php-ext-* scripts and simplifies the installation of PHP extensions by automatically adding and removing Debian (apt) and Alpine (apk) packages. For example, to install the GD extension you simply have to run install-php-extensions gd. This tool is contributed by community members and is not included in the images, please refer to their Git repository for installation, usage, and issues.

# https://github.com/mlocati/docker-php-extension-installer
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions

RUN install-php-extensions gd intl opcache pdo_pgsql pgsql pdo_mysql zip xdebug soap xsl

RUN apk add --no-cache nginx bash git nodejs npm acl \
  && rm -rf /var/cache/apk/*

RUN mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.back
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
#COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY ./nginx/localhost.conf /etc/nginx/conf.d/localhost.conf

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
COPY ./php/my_php.ini "$PHP_INI_DIR/conf.d/"

RUN mkdir /www && mkdir /www/public
RUN echo "<?php echo phpinfo();" > /www/public/index.php

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN composer self-update

COPY ./entrypoint.sh /etc/entrypoint.sh
RUN chmod a+x /etc/entrypoint.sh
WORKDIR /www

ENTRYPOINT ["/etc/entrypoint.sh"]
